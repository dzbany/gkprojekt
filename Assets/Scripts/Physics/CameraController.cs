﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Rigidbody player;

    Vector3 baseOffset = new Vector3(0.0f, 4.0f, -16.0f);
    float cameraVerticalRotation = -10.0f;
    ControlsHandling controls;

    void Start()
    {
        controls = player.GetComponent<ControlsHandling>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 offset = controls.usedLookBack() ? -baseOffset : baseOffset;

        if (player.velocity.magnitude < 0.5f)
        {
            transform.position = player.position
            + Vector3.up * offset.y
            + player.transform.forward * offset.z;

            correctCameraHeight(offset);

            transform.LookAt(player.transform);
            transform.RotateAround(transform.position, transform.right, cameraVerticalRotation);
        }
        else
        {
            Vector3 playerVelocity = player.velocity.normalized;
            playerVelocity.y = 0.0f;
            playerVelocity.Normalize();

            transform.position = player.position
                + Vector3.up * offset.y
                + playerVelocity * offset.z;

            correctCameraHeight(offset);

            transform.LookAt(player.transform);
            transform.RotateAround(transform.position, transform.right, cameraVerticalRotation);
        }
    }

    void correctCameraHeight(Vector3 offset)
    {
        float terrainHeightAtCameraPosition = player.GetComponent<TerrainGenerator>().GetHeightGlobal(transform.position.x, transform.position.z);
        if (transform.position.y < terrainHeightAtCameraPosition + offset.y)
        {
            transform.position = new Vector3(transform.position.x, terrainHeightAtCameraPosition + offset.y, transform.position.z);
        }
    }
}
