﻿using UnityEngine;
public class ControlsHandling : MonoBehaviour
{
    //Mouse sensitivity
    public float mouseSensitivity = 0.1f;
    //Keyboard sensitivity
    public float keyboardSensitivity = 10f;

    //Controller enabled?
    private bool usingController;

    float keyboardTurnAngle = 0f;

    void Start()
    {
        Cursor.visible = false;
    }

    //Checks if mouse and pad control should be enabled or disabled
    void Update()
    {
        usingController = Input.GetJoystickNames().Length > 0 && Input.GetJoystickNames()[0].Length > 0;
    }

    //Checks if player used PUSH.
    public bool usedPush()
    {
        return Input.GetButton("Push (keyboard)") || usingController && Input.GetButton("Push (controller)") || Input.GetButton("Push (mouse)");
    }

    //Returns value if player is turning (from -1(left) to 1 (right))
    public float usedTurn(float deltaTime)
    {
        float controllerTurnAngle = usingController ? Input.GetAxis("Turn (controller)") : 0f;

        float mouseTurnAngle = Input.GetAxis("Turn (mouse movement)") * mouseSensitivity;

        if (Input.GetButton("Turn left (keyboard)")) keyboardTurnAngle -= deltaTime * keyboardSensitivity;
        if (Input.GetButton("Turn right (keyboard)")) keyboardTurnAngle += deltaTime * keyboardSensitivity;
        if(!Input.GetButton("Turn left (keyboard)") && !Input.GetButton("Turn right (keyboard)")) keyboardTurnAngle *= 0.1f * deltaTime;
        keyboardTurnAngle = Mathf.Clamp(keyboardTurnAngle, -1f, 1f);

        return keyboardTurnAngle + controllerTurnAngle + mouseTurnAngle;
    }

    //Checks if player used leaning forward
    public bool usedLeaning()
    {
        return Input.GetButton("Lean forward (keyboard)") || usingController && Input.GetAxis("Lean forward (controller)") != 0 || Input.GetButton("Lean forward (mouse)");
    }

    //Checks if player used braking
    public bool usedBraking()
    {
        return Input.GetButton("Brake (keyboard)") || usingController && Input.GetAxis("Brake (controller)") != 0 || Input.GetButton("Brake (mouse)");
    }

    //Checks if player used look back
    public bool usedLookBack()
    {
        return Input.GetButton("Look back (keyboard)") || usingController && Input.GetButton("Look back (controller)") || Input.GetButton("Look back (mouse)");
    }
}