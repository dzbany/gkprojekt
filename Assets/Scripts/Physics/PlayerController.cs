using UnityEngine;
using UnityEngine.SceneManagement;
//IMPORTANT
// Player's rigidbody should have drag set to 0, player's physics material should have both frictions set to 0
public class PlayerController : MonoBehaviour
{
    Rigidbody capsuleRigidbody;
    Collider capsuleCollider;
    ControlsHandling controls;
    public GameObject leftSki;
    ParticleSystem leftSkiParticles;
    public GameObject rightSki;
    ParticleSystem rightSkiParticles;

    // Maximum velocity that allows player to use ski poles
    public float maxSkiPolesVelocity = 30.0f;  // probably 30m/s
    // Ski poles push force
    float skiPolesForce;
    // Time contraint between ski poles uses
    public float skiPolesTimeCostraint = 1.0f;  // 1s
    // Time elapsed from last ski poles use
    float timeFromLastSkiPolesUse;
    // Number of ski poles uses to achieve max velocity
    public float skiPolesUsesToMaxVelocity = 5;
    // Player turning force
    public float turningForce = 25f;
    // Coefficient of player braking
    public float brakingCoefficient = 0.95f;
    // Anisotropic friction coefficient
    public Vector3 anisotropicFrictionCoefficient;
    // Base forward drag
    public float baseForwardDrag = 0.1f;
    // Anisotropic drag coefficient
    public Vector3 anisotropicDragCoefficient;
    // Is player grounded?
    bool playerGrounded = true;

    //INPUT VARIABLES
    // Is player using ski poles?
    bool playerPushing = false;
    // Is player turning, in what direction, and how much?
    float playerTurning = 0.0f;
    // Is player leaning?
    bool playerLeaning = false;
    // Is player braking?
    bool playerBraking = false;

    float directionAngle = 0.0f;
    Quaternion lastRotationBeforeJump;

    float sideLeanAngle = 0.0f;
    float maxSideLeanAngle = 45.0f;
    float forwardLeanAngle = 0.0f;
    float maxForwardLeanAngle = 45.0f;
    float leaningSpeed = 2.0f;

    Vector3 leftSkiBaseLocalPosition;
    Vector3 rightSkiBaseLocalPosition;
    Quaternion skiBaseRotation;

    // Start is called before the first frame update
    void Start()
    {
        timeFromLastSkiPolesUse = skiPolesTimeCostraint;

        controls = GetComponent<ControlsHandling>();
        capsuleRigidbody = GetComponent<Rigidbody>();
        capsuleCollider = GetComponent<Collider>();

        // Value adjusted to achieve maxSkiPolesVelocity in skiPolesUsesToMaxVelocity
        skiPolesForce = capsuleRigidbody.mass * maxSkiPolesVelocity / (skiPolesUsesToMaxVelocity * skiPolesTimeCostraint);

        anisotropicFrictionCoefficient = new Vector3(3.0f, 0.0f, 0.005f);
        anisotropicDragCoefficient = new Vector3(0.1f, 0.1f, baseForwardDrag);

        GetComponent<TerrainGenerator>().PrepareSpawnArea();
        transform.position = new Vector3(0, GetComponent<TerrainGenerator>().GetHeightGlobal(0, 0) + 1, 0);

        leftSkiBaseLocalPosition = leftSki.transform.localPosition;
        rightSkiBaseLocalPosition = rightSki.transform.localPosition;
        skiBaseRotation = leftSki.transform.localRotation;
    }

    // Update is called once per frame
    void Update()
    {
        timeFromLastSkiPolesUse += Time.deltaTime;

        if (controls.usedPush() && timeFromLastSkiPolesUse > skiPolesTimeCostraint)
        {
            playerPushing = true;
            timeFromLastSkiPolesUse = 0.0f;
        }
        else
        {
            playerPushing = false;
        }

        playerLeaning = controls.usedLeaning();
        playerBraking = controls.usedBraking();
        playerTurning = controls.usedTurn(Time.deltaTime);
    }

    // FixedUpdate is called 50 times/second
    void FixedUpdate()
    {
        playerGrounded = IsGrounded();

        AdjustMovement();
        AdjustRotation();
        ApplyLean();
        ApplyFriction();
        ApplyDrag();
        ControlParticles();

        //DEBUG
        //Global V is in global coordinate system. Relative V is in player's coordinate system
        //Debug.Log("Global V = " + capsuleRigidbody.velocity + " Relative V = " + transform.InverseTransformDirection(capsuleRigidbody.velocity) + " |V| = " + capsuleRigidbody.velocity.magnitude + " Is grounded: " + playerGrounded + " Local euler z angle: " + transform.localEulerAngles.z);
    }

    //Checks if the player is touching the ground
    private bool IsGrounded()
    {
        if (Physics.Raycast(transform.position, Vector3.down, capsuleCollider.bounds.extents.y + 0.1f))
            return true;
        else if (Physics.Raycast(transform.position + new Vector3(-capsuleCollider.bounds.extents.x / 2f, 0f, -capsuleCollider.bounds.extents.z / 2f), Vector3.down, capsuleCollider.bounds.extents.y + 0.1f))
            return true;
        else if (Physics.Raycast(transform.position + new Vector3(capsuleCollider.bounds.extents.x / 2f, 0f, -capsuleCollider.bounds.extents.z / 2f), Vector3.down, capsuleCollider.bounds.extents.y + 0.1f))
            return true;
        else if (Physics.Raycast(transform.position + new Vector3(-capsuleCollider.bounds.extents.x / 2f, 0f, capsuleCollider.bounds.extents.z / 2f), Vector3.down, capsuleCollider.bounds.extents.y + 0.1f))
            return true;
        else if (Physics.Raycast(transform.position + new Vector3(capsuleCollider.bounds.extents.x / 2f, 0f, capsuleCollider.bounds.extents.z / 2f), Vector3.down, capsuleCollider.bounds.extents.y + 0.1f))
            return true;
        return false;
    }

    // Calculates rotation in XZ plane
    private void UpdateDirection()
    {
        float linearSpeed = Mathf.Min(capsuleRigidbody.velocity.magnitude, 40.0f);
        float angle = 90.0f * Time.deltaTime * linearSpeed / 40.0f * (IsGrounded() ? 1.0f : 0.2f);
        directionAngle += playerTurning * angle;
    }

    // Calculates side lean angle
    private void UpdateSideLeanAngle()
    {
        float angle = -capsuleRigidbody.velocity.magnitude * 0.75f;
        sideLeanAngle += playerTurning * angle * Time.deltaTime * leaningSpeed;

        if (sideLeanAngle > maxSideLeanAngle)
        {
            sideLeanAngle = maxSideLeanAngle;
        }
        else if (sideLeanAngle < -maxSideLeanAngle)
        {
            sideLeanAngle = -maxSideLeanAngle;
        }

        if (playerGrounded)
        {
            sideLeanAngle *= 0.9f;
        }
    }

    // Calculates forward lean angle
    private void UpdateForwardLeanAngle()
    {
        if (playerLeaning)
        {
            //float angle = capsuleRigidbody.velocity.magnitude * 0.75f;
            forwardLeanAngle += 45 * Time.deltaTime * leaningSpeed;
            if (forwardLeanAngle > maxForwardLeanAngle)
            {
                forwardLeanAngle = maxForwardLeanAngle;
            }
        }
        else
        {
            forwardLeanAngle -= 45 * Time.deltaTime * leaningSpeed;
            if (forwardLeanAngle < 0)
            {
                forwardLeanAngle = 0;
            }
        }
    }

    // Anisotropic friction implementation
    private void ApplyFriction()
    {
        if (playerGrounded)
        {
            Vector3 playerRelativeVelocity = transform.InverseTransformDirection(capsuleRigidbody.velocity);
            playerRelativeVelocity.x -= playerRelativeVelocity.x * anisotropicFrictionCoefficient.x * Time.fixedDeltaTime;
            playerRelativeVelocity.z -= playerRelativeVelocity.z * anisotropicFrictionCoefficient.z * Time.fixedDeltaTime;
            capsuleRigidbody.velocity = transform.TransformDirection(playerRelativeVelocity);
        }
    }

    // Anisotropic drag (air resistance) implementation
    private void ApplyDrag()
    {
        Vector3 playerRelativeVelocity = transform.InverseTransformDirection(capsuleRigidbody.velocity);
        playerRelativeVelocity.x *= (1 - anisotropicDragCoefficient.x * Time.fixedDeltaTime);
        playerRelativeVelocity.y *= (1 - anisotropicDragCoefficient.y * Time.fixedDeltaTime);
        playerRelativeVelocity.z *= (1 - anisotropicDragCoefficient.z * Time.fixedDeltaTime);
        capsuleRigidbody.velocity = transform.TransformDirection(playerRelativeVelocity);
    }

    // Adjusts player's global rotation to match terrain shape
    private void AdjustRotation()
    {
        UpdateDirection();

        if (playerGrounded)
        {
            Vector3 vec1, vec2;
            vec1 = new Vector3(0, GetComponent<TerrainGenerator>().GetHeightGlobal(transform.position.x, transform.position.z + 0.625f) - GetComponent<TerrainGenerator>().GetHeightGlobal(transform.position.x, transform.position.z - 0.625f), 2.5f);
            vec2 = new Vector3(1, GetComponent<TerrainGenerator>().GetHeightGlobal(transform.position.x + 0.25f, transform.position.z) - GetComponent<TerrainGenerator>().GetHeightGlobal(transform.position.x - 0.25f, transform.position.z), 0);
            Vector3 vec3 = Vector3.Cross(vec1, vec2);
            Quaternion quat = new Quaternion(vec3.x, vec3.y, vec3.z, 0);
            lastRotationBeforeJump = quat;
        }

        transform.rotation = lastRotationBeforeJump;
        leftSki.transform.localPosition = leftSkiBaseLocalPosition;
        rightSki.transform.localPosition = rightSkiBaseLocalPosition;
        leftSki.transform.localRotation = skiBaseRotation;
        rightSki.transform.localRotation = skiBaseRotation;
        transform.RotateAround(transform.position, transform.up, directionAngle);
    }

    // Applies leaning
    private void ApplyLean()
    {
        UpdateSideLeanAngle();
        UpdateForwardLeanAngle();

        transform.RotateAround(transform.position, transform.forward, sideLeanAngle);

        if (playerGrounded)
        {
            // Placing skis on the ground
            float leftSkiGroundLevel = GetComponent<TerrainGenerator>().GetHeightGlobal(leftSki.transform.position.x, leftSki.transform.position.z);
            float rightSkiGroundLevel = GetComponent<TerrainGenerator>().GetHeightGlobal(rightSki.transform.position.x, rightSki.transform.position.z);

            leftSki.transform.position = new Vector3(leftSki.transform.position.x, leftSkiGroundLevel, leftSki.transform.position.z);
            rightSki.transform.position = new Vector3(rightSki.transform.position.x, rightSkiGroundLevel, rightSki.transform.position.z);
        }

        transform.RotateAround(transform.position, transform.right, forwardLeanAngle);
        leftSki.transform.RotateAround(transform.position, transform.right, -forwardLeanAngle);
        rightSki.transform.RotateAround(transform.position, transform.right, -forwardLeanAngle);
    }

    // Adjusts player's speed and forward air resistance
    private void AdjustMovement()
    {
        if (playerLeaning)
        {
            // Setting forward (z axis) drag coefficient to a fifth of the base value
            anisotropicDragCoefficient.z = baseForwardDrag / 5;
        }
        else
        {
            // Setting forward (z axis) drag coefficient to base value
            anisotropicDragCoefficient.z = baseForwardDrag;
        }

        if (playerPushing && playerGrounded && (capsuleRigidbody.velocity.magnitude < maxSkiPolesVelocity))
        {
            // Speeding up player in forward direction
            capsuleRigidbody.AddRelativeForce(Vector3.forward * skiPolesForce, ForceMode.Impulse);
        }
        else if (playerBraking)
        {
            // Slowing player's movement in forward direction
            // new player forward speed = old player forward speed * brakingCoefficient
            Vector3 playerRelativeVelocity = transform.InverseTransformDirection(capsuleRigidbody.velocity);
            playerRelativeVelocity.z *= brakingCoefficient;
            capsuleRigidbody.velocity = transform.TransformDirection(playerRelativeVelocity);
        }
    }

    private void ControlParticles()
    {

        ParticleSystem.ShapeModule sm1 = leftSki.GetComponent<ParticleSystem>().shape;
        ParticleSystem.ShapeModule sm2 = rightSki.GetComponent<ParticleSystem>().shape;
        Vector3 playerRelativeVelocity = transform.InverseTransformDirection(capsuleRigidbody.velocity);
        Vector3 rot = Quaternion.LookRotation(-playerRelativeVelocity).eulerAngles;
        sm1.rotation = rot;
        sm2.rotation = rot;

        ParticleSystem.MainModule mm1 = leftSki.GetComponent<ParticleSystem>().main;
        ParticleSystem.MainModule mm2 = rightSki.GetComponent<ParticleSystem>().main;
        float startSpeed = Mathf.Max(capsuleRigidbody.velocity.magnitude / 20, 1.5f);
        mm1.startSpeed = startSpeed;
        mm2.startSpeed = startSpeed;

        ParticleSystem.EmissionModule em1 = leftSki.GetComponent<ParticleSystem>().emission;
        ParticleSystem.EmissionModule em2 = rightSki.GetComponent<ParticleSystem>().emission;
        float rateOverDistance = (Mathf.Abs(180 - rot.y) - 5) / 175 * capsuleRigidbody.velocity.magnitude * 20 + capsuleRigidbody.velocity.magnitude;
        em1.rateOverDistance = rateOverDistance;
        em2.rateOverDistance = rateOverDistance;

        if (playerGrounded)
        {
            leftSki.GetComponent<ParticleSystem>().Play();
            rightSki.GetComponent<ParticleSystem>().Play();
        }
        else
        {
            leftSki.GetComponent<ParticleSystem>().Stop();
            rightSki.GetComponent<ParticleSystem>().Stop();
        }

        //Debug.Log(sm2.rotation.ToString());
    }

    void OnCollisionEnter(Collision col)
    {
        Vector3 collisionForce = col.impulse / Time.fixedDeltaTime;
        collisionForce.y = 0;

        if (collisionForce.magnitude > 30000)
        {
            Debug.Log("Game-ending collision detected");
            PointsHandler.points = (int)this.transform.position.z * (-1);
            Cursor.visible = true;
            SceneManager.LoadScene(2);
        }
    }
}
