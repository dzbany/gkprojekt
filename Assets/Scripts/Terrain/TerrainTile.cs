﻿using System;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;

public class TerrainTile
{
    GameObject tileObject = null;
    TerrainGenerator generator;
    long x;
    long y;
    float[,] heightData = new float[TerrainGenerator.resolution + 1, TerrainGenerator.resolution + 1];
    float[,,] alphamapData = new float[TerrainGenerator.resolution, TerrainGenerator.resolution, 2];
    //bool[,] treePlacement = new bool[TerrainGenerator.resolution, TerrainGenerator.resolution];
    bool heightmapGenerated = false;
    bool tilePlaced = false;

    TreePrototype[] treePrototypes = new TreePrototype[3];
    TerrainLayer[] terrainLayers = new TerrainLayer[2];
    List<TreeInstance> treeInstances = new List<TreeInstance>();

    public TerrainTile(TerrainGenerator generator, long x, long y, GameObject tree1, GameObject tree2, GameObject tree3, TerrainLayer snow, TerrainLayer rock, bool synchronous = false)
    {
        this.x = x;
        this.y = y;
        this.generator = generator;
        treePrototypes[0] = new TreePrototype();
        treePrototypes[1] = new TreePrototype();
        treePrototypes[2] = new TreePrototype();
        treePrototypes[0].prefab = tree1;
        treePrototypes[1].prefab = tree2;
        treePrototypes[2].prefab = tree3;
        terrainLayers[0] = rock;
        terrainLayers[1] = snow;

        if(synchronous)
        {
            GenerateHeightmap();
        }
        else
        {
            new Thread(new ThreadStart(GenerateHeightmap)).Start();
        }
    }

    private void GenerateHeightmap()
    {
        for (int i = 0; i < TerrainGenerator.resolution + 1; ++i)
        {
            for (int j = 0; j < TerrainGenerator.resolution + 1; ++j)
            {
                heightData[j, i] = generator.GetHeightRelative(i, x, j, y);
            }
        }

        System.Random rand = new System.Random();

        float scale = 0;
        float rotation = 0;
        int treeNb = 0;

        for (int i = 0; i < TerrainGenerator.resolution; ++i)
        {
            for (int j = 0; j < TerrainGenerator.resolution; ++j)
            {
                alphamapData[j, i, 0] = Mathf.Clamp01((float)(Math.Pow(heightData[j, i] - heightData[j, i + 1], 2) + Math.Pow(heightData[j, i] - heightData[j + 1, i], 2)) * 100000);
                alphamapData[j, i, 1] = 1 - alphamapData[j, i, 0];

                if(heightData[j, i] < .215f && generator.GetTreeDensityRelative(i, x, j, y) * .01f > rand.NextDouble())
                {
                    TreeInstance tree = new TreeInstance();
                    tree.prototypeIndex = treeNb;
                    tree.color = Color.white;
                    tree.lightmapColor = Color.white;
                    tree.heightScale = tree.widthScale = 1.5f + scale;
                    tree.rotation = rotation * 6.2832f;
                    tree.position = new Vector3((float)i / TerrainGenerator.resolution, 0, (float)j / TerrainGenerator.resolution);
                    treeInstances.Add(tree);

                    scale = (scale + .542351f) % 1;
                    rotation = (rotation + .126745f) % 1;
                    treeNb = (treeNb + 1) % 3;
                }
            }
        }

        for (int i = 0; i < TerrainGenerator.resolution + 1; ++i)
        {
            for (int j = 0; j < TerrainGenerator.resolution + 1; ++j)
            {
                heightData[j, i] = generator.TiltHeight(heightData[j, i], j);
            }
        }

        heightmapGenerated = true;
    }

    public void PlaceTile()
    {
        if (!tilePlaced && heightmapGenerated)
        {
            TerrainData terrainData = new TerrainData
            {
                size = new Vector3(TerrainGenerator.resolution / 4, generator.height * (1 + generator.slope), TerrainGenerator.resolution / 4),
                heightmapResolution = TerrainGenerator.resolution + 1,
                alphamapResolution = TerrainGenerator.resolution,
                terrainLayers = terrainLayers,
                treePrototypes = treePrototypes,
                treeInstances = treeInstances.ToArray()
            };
            terrainData.SetHeights(0, 0, heightData);
            terrainData.SetAlphamaps(0, 0, alphamapData);

            tileObject = Terrain.CreateTerrainGameObject(terrainData);
            tileObject.transform.position = new Vector3(x * TerrainGenerator.resolution, TerrainGenerator.resolution * generator.slope * y, y * TerrainGenerator.resolution);

            tilePlaced = true;
        }
    }
    
    public void Dispose()
    {
        UnityEngine.Object.Destroy(tileObject);
        tileObject = null;
    }
}
