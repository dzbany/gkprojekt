﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    Dictionary<long, TerrainTile> terrainTiles = new Dictionary<long, TerrainTile>();

    public GameObject tree1;
    public GameObject tree2;
    public GameObject tree3;

    public TerrainLayer snow;
    public TerrainLayer rock;

    public int radius = 6;
    public int scale = 10;
    public float height = 500;
    public float slope = 0.3f;

    public const int resolution = 128;

    private long baseX;
    private long baseY;

    void Start()
    {
        float h1, h2, h3, h4, h5;
        int attempts = 0;
        do
        {
            baseX = (long)(UnityEngine.Random.value * 200000 + 100000);
            baseY = (long)(UnityEngine.Random.value * 200000 + 100000);
            ++attempts;
            h1 = GetHeightGlobal(0, 0);
            h2 = GetHeightGlobal(0, -100.0f);
            h3 = GetHeightGlobal(0, -200.0f);
            h4 = GetHeightGlobal(0, -300.0f);
            h5 = GetHeightGlobal(0, -400.0f);
        } while (!(h1 < 100.0f && h2 < h1 && h3 < h2 && h4 < h3 && h5 < h4));
        Debug.Log("Terrain base coordinates: x=" + baseX + ", y=" + baseY + ", found after " + attempts + " attempts.");
    }

    public void PrepareSpawnArea()
    {
        AddTile(0, 0, true);
        AddTile(0, -1, true);
        AddTile(-1, 0, true);
        AddTile(-1, -1, true);
    }

    void Update()
    {
        float originXf = transform.position.x / resolution;
        float originYf = transform.position.z / resolution;
        long originX = Mathf.FloorToInt(originXf);
        long originY = Mathf.FloorToInt(originYf);
        long tileX = originX;
        long tileY = originY;

        for (long x = tileX - radius - 1; x < tileX + radius + 2; ++x)
        {
            for (long y = tileY - radius - 1; y < tileY + radius + 2; ++y)
            {
                if(x == tileX - radius - 1 || x == tileX + radius + 1 || y == tileY - radius - 1 || y == tileY + radius + 1)
                {
                    RemoveTile(x, y);
                }
                else if(Mathf.Pow(x - originXf, 2) + Mathf.Pow(y - originYf, 2) < Mathf.Pow(radius, 2))
                {
                    AddTile(x, y);
                }
            }
        }
    }

    private void AddTile(long x, long y, bool placeImmediately = false)
    {
        long tilePos = xy2d(x, y);

        if (terrainTiles.ContainsKey(tilePos))
        {
            terrainTiles[tilePos].PlaceTile();
        }
        else
        {
            terrainTiles[tilePos] = new TerrainTile(this, x, y, tree1, tree2, tree3, snow, rock, placeImmediately);
            if(placeImmediately)
            {
                terrainTiles[tilePos].PlaceTile();
            }
        }
    }

    private void RemoveTile(long x, long y)
    {
        long tilePos = xy2d(x, y);

        if (terrainTiles.ContainsKey(tilePos))
        {
            terrainTiles[tilePos].Dispose();
            terrainTiles.Remove(tilePos);
        }
    }

    public float GetHeightGlobal(float x, float y)
    {
        return GetHeightNoise(x, y) * height + y * slope;
    }

    public float GetHeightRelative(long x, long tileX, long y, long tileY)
    {
        return GetHeightNoise(x + tileX * resolution, y + tileY * resolution);
    }

    public float TiltHeight(float heightRelative, long y)
    {
        return (heightRelative + y * slope / height) / (1 + slope);
    }

    public float GetHeightNoise(float x, float y)
    {
        x += baseX;
        y += baseY;

        float pebbles1 = Perlin.Noise(x, y, .8f * scale) * .3f;
        float pebbles2 = Perlin.Noise(x, y, 1.3f * scale) * .4f;
        float boulders1 = Perlin.Noise(x, y, 7 * scale) * .1f;
        float boulders2 = Perlin.Noise(x, y, 9 * scale) * .05f;
        float rocks = (float)Math.Pow(pebbles1, 3) + (float)Math.Pow(pebbles2, 3) + boulders1 + boulders2;
        float generalShape = (float)Math.Pow(.1f + Perlin.Noise(x, y, 100 * scale) * 1.2f, 2);
        float snowRocksBorderNoise = Perlin.Noise(x, y, 27 * scale) * .08f;
        return Math.Max(.1f + generalShape, rocks + generalShape * 1.6f + snowRocksBorderNoise);
    }

    public float GetTreeDensityRelative(long x, long tileX, long y, long tileY)
    {
        return GetTreeDensityNoise(x + tileX * resolution, y + tileY * resolution);
    }

    public float GetTreeDensityNoise(float x, float y)
    {
        x += baseX;
        y += baseY;

        return Mathf.Clamp01(Perlin.Noise(x, y, 41 * scale) * 1.8f + Perlin.Noise(x, y, 23 * scale) * 1f);
    }

        private static long sq(long n)
    {
        return n * n;
    }

    private static long xy2d(long x, long y)
    {
        if (x == 0 && y == 0)
        {
            return 0;
        }
        else if (-y <= x && y < x)
        {
            return sq(x * 2 - 1) + x + y;
        }
        else if (-x < y)
        {
            return sq(y * 2 - 1) + y * 3 - x;
        }
        else if (y > x)
        {
            return sq(x * 2 + 1) - x * 5 - y;
        }
        else
        {
            return sq(y * 2 + 1) - y * 7 + x;
        }
    }
}
