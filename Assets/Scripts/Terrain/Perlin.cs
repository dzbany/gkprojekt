﻿using UnityEngine;

public static class Perlin
{
    public static float Noise(float x, float y, float scale)
    {
        x /= scale;
        y /= scale;

        long x0 = Mathf.FloorToInt(x);
        long x1 = x0 + 1;
        long y0 = Mathf.FloorToInt(y);
        long y1 = y0 + 1;

        float sx = x - x0;
        float sy = y - y0;

        float n0 = DotGridGradient(x0, y0, x, y);
        float n1 = DotGridGradient(x1, y0, x, y);
        float ix0 = Interpolate(n0, n1, sx);
        n0 = DotGridGradient(x0, y1, x, y);
        n1 = DotGridGradient(x1, y1, x, y);
        float ix1 = Interpolate(n0, n1, sx);

        return Interpolate(ix0, ix1, sy);
    }

    private static float[,] gradients = new float[23, 2] {
        { Mathf.Sin((4f / 23f) * Mathf.PI * 2f), Mathf.Cos((4f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((0f / 23f) * Mathf.PI * 2f), Mathf.Cos((0f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((5f / 23f) * Mathf.PI * 2f), Mathf.Cos((5f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((16f / 23f) * Mathf.PI * 2f), Mathf.Cos((16f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((20f / 23f) * Mathf.PI * 2f), Mathf.Cos((20f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((9f / 23f) * Mathf.PI * 2f), Mathf.Cos((9f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((14f / 23f) * Mathf.PI * 2f), Mathf.Cos((14f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((11f / 23f) * Mathf.PI * 2f), Mathf.Cos((11f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((2f / 23f) * Mathf.PI * 2f), Mathf.Cos((2f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((21f / 23f) * Mathf.PI * 2f), Mathf.Cos((21f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((8f / 23f) * Mathf.PI * 2f), Mathf.Cos((8f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((17f / 23f) * Mathf.PI * 2f), Mathf.Cos((17f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((12f / 23f) * Mathf.PI * 2f), Mathf.Cos((12f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((6f / 23f) * Mathf.PI * 2f), Mathf.Cos((6f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((13f / 23f) * Mathf.PI * 2f), Mathf.Cos((13f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((10f / 23f) * Mathf.PI * 2f), Mathf.Cos((10f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((15f / 23f) * Mathf.PI * 2f), Mathf.Cos((15f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((1f / 23f) * Mathf.PI * 2f), Mathf.Cos((1f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((18f / 23f) * Mathf.PI * 2f), Mathf.Cos((18f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((3f / 23f) * Mathf.PI * 2f), Mathf.Cos((3f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((7f / 23f) * Mathf.PI * 2f), Mathf.Cos((7f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((22f / 23f) * Mathf.PI * 2f), Mathf.Cos((22f / 23f) * Mathf.PI * 2f) },
        { Mathf.Sin((19f / 23f) * Mathf.PI * 2f), Mathf.Cos((19f / 23f) * Mathf.PI * 2f) }
    };

    private static float Interpolate(float a0, float a1, float w)
    {
        w = w * w * w * (w * (w * 6 - 15) + 10);

        return a0 + w * (a1 - a0);
    }

    private static float DotGridGradient(long ix, long iy, float x, float y)
    {
        float dx = x - ix;
        float dy = y - iy;
        long d = xy2d(ix, iy) % 23;
        return dx * gradients[d, 0] + dy * gradients[d, 1];
    }

    private static long xy2d(long x, long y)
    {
        if (x == 0 && y == 0)
        {
            return 0;
        }
        else if (-y <= x && y < x)
        {
            return sq(x * 2 - 1) + x + y;
        }
        else if (-x < y)
        {
            return sq(y * 2 - 1) + y * 3 - x;
        }
        else if (y > x)
        {
            return sq(x * 2 + 1) - x * 5 - y;
        }
        else
        {
            return sq(y * 2 + 1) - y * 7 + x;
        }
    }

    private static long sq(long n)
    {
        return n * n;
    }
}
