﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class AvalancheController : MonoBehaviour
{
    public float speed = 0.3f;
    public float maxDistance = 300.0f;
    public GameObject player;

    TerrainGenerator gen = null;
    Collider playerCollider = null;

    void Start()
    {
        playerCollider = player.GetComponent<CapsuleCollider>();
        gen = player.GetComponent<TerrainGenerator>();
    }

    void Update()
    {
        Vector3 pos = transform.position;
        float movement = speed * Time.deltaTime;
        if(pos.z - player.transform.position.z > maxDistance)
        {
            movement = pos.z - player.transform.position.z - maxDistance;
        }
        pos.z -= movement;
        pos.y -= movement * gen.slope;
        pos.x = player.transform.position.x;
        transform.position = pos;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other == playerCollider)
        {
            Debug.Log("Collision with avalanche detected");
            PointsHandler.points = (int)player.transform.position.z * (-1);
            Cursor.visible = true;
            SceneManager.LoadScene(2);
        }
    }
}
