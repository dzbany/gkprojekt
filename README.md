# GKprojekt

## Gałęzie:

- ui - menu, pauza, HUD, punktacja itd; o wstawienie dodatkowych elementów do świata prosić Piotrka, o informacje dotyczące prędkości/położenia/odległości prosić Roberta

- physics - fizyka gracza, elementy wizualne wokół gracza (ułożenie i wygląd nart, cząsteczki za nartami), kamera

- steering - integracja metod sterowania

- terrain - generowanie i teksturowanie terenu, wstawianie obiektów (skał, drzew) do świata, skybox, oświetlenie

Pracujcie nad elementami projektu na branchach i jak zacznie wam coś działać bez błędów, to merdżujcie z masterem; wtedy te zmiany będzie można z kolei zmerdżować z pozostałymi branchami.